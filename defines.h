#ifndef _DES_H_
#define _DES_H_
 
#define ENCRYPTION_MODE 1
#define DECRYPTION_MODE 0
 
typedef struct 
{
      unsigned char temp[8];
      unsigned char y[4];
      unsigned char z[4];
} key_set;
 
void generate_key(unsigned char* key);
void generate_sub_keys(unsigned char* main_key, key_set* key_sets);
void process_message(unsigned char* message_piece, unsigned char* processed_piece, key_set* key_sets, int mode);
 
#endif