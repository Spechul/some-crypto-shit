void extract_bits(unsigned char* bits, unsigned char symbol, int index);
int extract_to_array(unsigned char* bits, unsigned char* array, int len_bits, int len_array); // sizeof(bits) == 8 * sizeof(array)
void pack(unsigned char* source, unsigned int* dest);