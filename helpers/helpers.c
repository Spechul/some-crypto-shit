#include "helpers.h"


void extract_bits(unsigned char* bits, unsigned char symbol, int index)
{
    for(int i = index + 7; i > index; i--)
    {
        bits[i] = symbol & 0b1;
        symbol = symbol >> 1;
    }
}

int extract_to_array(unsigned char* bits, unsigned char* array, int len_bits, int len_array) // sizeof(bits) == 8 * sizeof(array)
{
    if (len_bits != 8 * len_array)
        return -1;

    for(int i = 0, j = 0; i < len_array; i++, j += 8)
    {
        extract_bits(bits, array[i], j);
    }
    return 0;
}

unsigned int pack_symbol(unsigned int c0, unsigned int c1, unsigned int c2, unsigned int c3) 
{
    return (c0 << 24) | (c1 << 16) | (c2 << 8) | c3;
}

void pack(unsigned char* source, unsigned int* dest)
{
    int source_len = sizeof (source) / sizeof(char);
    int dest_len = sizeof(dest) / sizeof(int);
    for (int i = 0, j = 0; i < dest_len; i++, j+=4)
    {
        dest[i] = pack_symbol(source[j], source[j + 1], source[j + 2], source[j + 3]);
    }
}
