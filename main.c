#include <stdio.h>
#include <conio.h>
#include "des/des.h"
#include "helpers/helpers.h"
#include "des1.c"

int main()
{
    unsigned char main_keys[8];
    make_keys(main_keys);
    key_set keys[16];
    key_maker(main_keys, keys);
    //display_keys(keys[0]);
    unsigned char message[8] = {1, 0, 1, 0, 1, 0, 1, 0};
    unsigned char res[8];
    unsigned char res2[8];
    evaluate_message(message, res, keys, ENCRYPTION_MODE);
    printf("encrypt\n");
    for (int i = 0; i < 8; i++)
    {
        binary_digits(res[i]);
        printf("\n");
    }
    printf("\n\n");
    evaluate_message(res, res2, keys, DECRYPTION_MODE);
    printf("decrypt\n");
    for (int i = 0; i < 8; i++)
    {
        binary_digits(res2[i]);
        printf("\n");
    }

    //printf("%s\n", res2);
    getch();
    return 0;
}